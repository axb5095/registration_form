import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./components/Register";
import {
  MDBBtn,
  MDBCardImage,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBSelect,
  MDBRadio
}
  from 'mdb-react-ui-kit';

function App() {
  const style = {
    alignItems: "center",
    boxShadow: '1px 2px 9px #F4AAB9',
    borderRadius: 20,

  }
  const all = {
    alignItems: "center",
    boxShadow: '1px 2px 9px #F4AAB9',
    borderRadius: 20,
  

  }
  return (
    <div class="style">
      {/* <Form style={{ width: "40%", alignItems: "center" }}>
        <Form.Group className="mb-4" controlId="formBasicEmail">
          <Form.Label>First Name</Form.Label>
          <Form.Control type="text" placeholder="Enter First Name" />
          <Form.Text className="text-muted">
            We'll never share your Name with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-2" controlId="formBasicPassword">
          <Form.Label>Last Name</Form.Label>
          <Form.Control type="text" placeholder="Enter Your Last Name" />
        </Form.Group>


        <Form.Group className="mb-4" controlId="formBasicEmail">
          <Form.Label>Age</Form.Label>
          <Form.Control type="number" placeholder="Enter your Age" />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>



        <Form.Group className="mb-4" controlId="formBasicEmail">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="******************" />
          <Form.Text className="text-muted">
            We'll never share your password with anyone else.
          </Form.Text>
        </Form.Group>


        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <select name="" id="">
            <option type="checkbox" value="Male">Male</option>
            <option type="checkbox" value="Female">Female</option>
          </select>
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form> */}

      <MDBContainer fluid className='bg-light'>

        <MDBRow className='d-flex justify-content-center align-items-center h-100' style={all}>
          <MDBCol>

            <MDBCard className='my-4'>

              <MDBRow className='g-0'>

                <MDBCol md='' className="d-none d-md-block">
                  <MDBCardImage src='https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/img4.webp' alt="Sample photo" className="rounded-start" fluid />
                </MDBCol>

                <MDBCol md='6' style={style}>

                  <MDBCardBody className='text-black d-flex flex-column justify-content-center'>
                    <h3 className="mb-5 text-uppercase fw-bold">Registration Form</h3>

                    <MDBRow>

                      <MDBCol md='6'>
                        <MDBInput wrapperClass='mb-4' label='First Name' size='lg' id='form1' type='text' />
                      </MDBCol>

                      <MDBCol md='6'>
                        <MDBInput wrapperClass='mb-4' label='Last Name' size='lg' id='form2' type='text' />
                      </MDBCol>

                    </MDBRow>

                    <MDBInput wrapperClass='mb-4' label='Age' size='lg' id='form3' type='number' />

                    <MDBInput wrapperClass='mb-4' label='Email' size='lg' id='form4' type='email' />
                    <MDBInput wrapperClass='mb-4' label='Password' size='lg' id='form5' type='password' />
                    <div className='d-md-flex ustify-content-start align-items-center mb-4'>
                      <h6 class="fw-bold mb-0 me-4">Gender: </h6>
                      <MDBRadio name='inlineRadio' id='inlineRadio1' value='option1' label='Female' inline />
                      <MDBRadio name='inlineRadio' id='inlineRadio2' value='option2' label='Male' inline />
                      <MDBRadio name='inlineRadio' id='inlineRadio3' value='option3' label='Other' inline />
                    </div>

                    <MDBRow>

                      <MDBCol md='6'>
                      </MDBCol>

                      <MDBCol md='6'>

                      </MDBCol>
                    </MDBRow>
                    <div className="d-flex justify-content-end pt-3">
                      <MDBBtn className='ms-2' color='warning' size='lg'>Register</MDBBtn>
                    </div>

                  </MDBCardBody>

                </MDBCol>
              </MDBRow>

            </MDBCard>

          </MDBCol>
        </MDBRow>

      </MDBContainer>

    </div>
  );
}

export default App;
